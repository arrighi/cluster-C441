for i in {01..23};
do
    if [ ! $(hostname) = $i ];
    then
        rsync -e ssh -avz --delete-after $1$2 $i.local:$1
    fi
done
