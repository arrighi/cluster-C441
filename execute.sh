for i in {01..23};
do
    if [ ! $(hostname) = $i ];
    then
        ssh $i.dptinfo $* &
    fi
done
